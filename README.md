# README #

SOAapp basic SOA architecture:
* Server side Spring 4
* Database H2 in memory, populated at startup
* Client side Angular 1
Applications can be deployed on different servers

### Debug ###
For debugging filter application [org.springframework.security.web.FilterChainProxy].doFilterInternal

### Url ###
http://localhost:port/Demo/static/site (inside Eclipse)

### User credentials ###
demouser/pw

### TODO ###
* package client application with bower/gulp
package it.demotest.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import com.fasterxml.jackson.databind.type.TypeFactory;

import it.demo.model.Product;
import it.demotest.context.AbstractWebApplictionContext;

public class RestEndpointTest extends AbstractWebApplictionContext {

	@Test
	public void testCustomerWithLocation() throws Exception {
		mockMvc
			.perform(buildAuthenticatedGet("/api/getLocationIdByCustomerId")
						.param("customerId","1800")) 
			.andExpect(status().isOk()).andExpect(content().contentType(APPLICATION_JSON_UTF8));
	}

	@Test
	public void testLocationProblem() throws Exception {
		mockMvc
			.perform(buildAuthenticatedGet("/api/getLocationIdByCustomerId").param("customerId","-1")) 
			.andExpect(status().isNotFound());
	}
	
	@Test
	public void testProducts() throws Exception {
		MockHttpServletResponse response = mockMvc
			.perform(buildAuthenticatedGet("/api/getProductsByLocationId").param("locationId","undefined")) 
			.andExpect(status().isOk()).andExpect(content().contentType(APPLICATION_JSON_UTF8))
			.andReturn().getResponse();

		List<Product> products = mapper.readValue(response.getContentAsString(), TypeFactory.defaultInstance().constructCollectionType(List.class, Product.class));
		assertEquals(products.size(), 2);
	}
	
}

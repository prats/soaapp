package it.demotest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.demo.model.ext.Credentials;
import it.demotest.context.AbstractWebApplictionContext;

public class RestPublicEndpointTest extends AbstractWebApplictionContext {
	
	private ResultActions loginCall(Credentials credentials) throws Exception, JsonProcessingException {
		return 
			mockMvc
				.perform(
						post("/api/public/login")
						.content(mapper.writeValueAsString(credentials))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						)
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));
	}

	@Test
	public void testLoginFailure() throws Exception {
		Credentials credentials = new Credentials();
		credentials.setUsername("xx");
		credentials.setPassword("yy");
		loginCall(credentials) 
			.andExpect(status().isUnauthorized());
	}
	
	@Test
	public void testLoginSuccess() throws Exception {
		Credentials credentials = new Credentials();
		credentials.setUsername(DEMO_USER);
		credentials.setPassword(DEMO_PW);
		loginCall(credentials) 
			.andExpect(status().isOk());
	}
	
	private ResultActions registerCall(Credentials credentials) throws Exception, JsonProcessingException {
		return 
			mockMvc
				.perform(
						post("/api/public/register")
						.content(mapper.writeValueAsString(credentials))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						)
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));
	}
	
	@Test
	public void testRegisterUserSuccess() throws Exception {
		Credentials credentials = new Credentials();
		credentials.setUsername("newuser");
		credentials.setPassword("newpw");

		registerCall(credentials)
			.andExpect(status().isOk());
	}
	
	@Test
	public void testRegisterUserFailure() throws Exception {
		Credentials credentials = new Credentials();
		credentials.setUsername("newuser");
		credentials.setPassword("");

		registerCall(credentials)
			.andExpect(status().isBadRequest());
	}
}

package it.demotest.conf;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import it.demo.configuration.IEnvConfiguration;
import it.demotest.context.AbstractWebApplictionContext;

public class SecurityConfigurationTest extends AbstractWebApplictionContext {
	
	//TODO find how to check cors is working
	
	@Test
	public void staticResourceReachability() throws Exception {
		mockMvc
			.perform(MockMvcRequestBuilders.get(IEnvConfiguration.URL_STATIC_RESOURCES + "/site")) 
			.andExpect(status().isOk());
	}
	
	@Test
	public void verifyDeniedAccessForPrivateUrl() throws Exception {
		mockMvc
			.perform(MockMvcRequestBuilders.get("/api/alive")) 
			.andExpect(status().isForbidden());
	}

	@Test
	public void verifyGrantedAccessWithTokenForPrivateUrl() throws Exception {
		mockMvc
			.perform(buildAuthenticatedGet("/api/alive"))
			.andExpect(status().isOk());
	}

	@Test
	public void verifyGrantedAccessForPublicUrl() throws Exception {
		mockMvc
			.perform(MockMvcRequestBuilders.get("/api/public/alive")) 
			.andExpect(status().isOk());
	}
}

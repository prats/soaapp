package it.demotest.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import it.demo.model.exception.FailureException;
import it.demo.service.CustomerLocationServiceStub;
import it.demo.service.ICustomerLocationService;
import it.demotest.context.TestConfiguration;

public class CustomerLocationServiceTest extends TestConfiguration {

	@Autowired
	private ICustomerLocationService customerLocationService;
	
	@Test
	public void testLocationLondon() throws Exception {
		String customerLocationId = customerLocationService.getCustomerLocationId(2000);
		assertEquals(CustomerLocationServiceStub.LOCATION_LONDON, customerLocationId);
	}
	
	@Test
	public void testLocationLiverpool() throws Exception {
		String customerLocationId = customerLocationService.getCustomerLocationId(100);
		assertEquals(CustomerLocationServiceStub.LOCATION_LIVERPOOL, customerLocationId);
	}
	
	@Test(expected=FailureException.class)
	public void testLocationProblem() throws Exception {
		customerLocationService.getCustomerLocationId(-1);
	}
}

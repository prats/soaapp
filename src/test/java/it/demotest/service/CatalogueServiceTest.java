package it.demotest.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import it.demo.model.Product;
import it.demo.service.CustomerLocationServiceStub;
import it.demo.service.ICatalogueService;
import it.demotest.context.TestConfiguration;

public class CatalogueServiceTest extends TestConfiguration {

	@Autowired
	private ICatalogueService catalogueService;
	
	private void checkOnNewsProducts(Set<String> productDescriptions) {
		assertTrue(productDescriptions.contains("Sky News"));
		assertTrue(productDescriptions.contains("Sky Sports News"));
	}
	
	private Set<String> extractProductsDescription(List<Product> products) {
		return products.stream().map(p -> p.getDescription()).collect(Collectors.toSet());
	}

	@Test
	public void testLondonProducts() throws Exception {
		List<Product> londonProducts = catalogueService.getProducts(CustomerLocationServiceStub.LOCATION_LONDON);
		assertEquals(londonProducts.size(), 4);
		Set<String> productDescriptions = extractProductsDescription(londonProducts);
		assertTrue(productDescriptions.contains("Arsenal TV"));
		assertTrue(productDescriptions.contains("Chelsea TV"));
		checkOnNewsProducts(productDescriptions);
	}
	
	@Test
	public void testLiverpoolProducts() throws Exception {
		List<Product> liverpoolProducts = catalogueService.getProducts(CustomerLocationServiceStub.LOCATION_LIVERPOOL);
		assertEquals(liverpoolProducts.size(), 3);
		Set<String> productDescriptions = extractProductsDescription(liverpoolProducts);
		assertTrue(productDescriptions.contains("Liverpool TV"));
		checkOnNewsProducts(productDescriptions);
	}
	
	@Test
	public void testUndefinedLocationProducts() throws Exception {
		List<Product> products = catalogueService.getProducts("undefined");
		assertEquals(products.size(), 2);
		Set<String> productDescriptions = extractProductsDescription(products);
		checkOnNewsProducts(productDescriptions);
	}
}

package it.demotest.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import it.demo.model.User;
import it.demo.model.exception.FailureException;
import it.demo.model.ext.LoginOutcome;
import it.demo.repositories.UserRepository;
import it.demo.security.TokenHandler;
import it.demo.service.IUserAuthService;
import it.demotest.context.TestConfiguration;

public class UserAuthServiceImplTest extends TestConfiguration {

	@Autowired
	private IUserAuthService userAuthService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TokenHandler tokenHandler;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private String username, password;

	@Before
	public void setUp() throws Exception {
		username = "testuser";
		password = "testpassword";
		userAuthService.registerUser(username, password);
	}

	@After
	public void tearDown() {
		User user = userRepository.findByUsername(username);
		userRepository.delete(user);
	}

	private LoginOutcome callSuccessfulLogin() {
		return userAuthService.login(username, password);
	}

	@Test
	public void loginSuccesfulTest() {
		LoginOutcome success = callSuccessfulLogin();
		assertTrue(success.isSuccess());
	}
	
	@Test
	public void loginJWTSuccesfulTest() {
		LoginOutcome success = callSuccessfulLogin();
		String username = tokenHandler.parseUserFromToken(success.getJwt());
		assertEquals(username, username);
	}
	
	private void checkUnsuccessfulLogin(LoginOutcome failure) {
		assertFalse(failure.isSuccess());
		assertTrue(failure.getMessage().contains("invalid"));
	}

	@Test
	public void loginUnsuccesfulWrongUserTest() {
		LoginOutcome failure = userAuthService.login("wronguser", "pw");
		checkUnsuccessfulLogin(failure);
		assertTrue(failure.getMessage().contains("user"));
	}
	
	@Test
	public void loginUnsuccesfulWrongPasswordTest() {
		LoginOutcome failure = userAuthService.login(username, "wrongpw");
		checkUnsuccessfulLogin(failure);
		assertTrue(failure.getMessage().contains("password"));
	}
	
	@Test
	public void registerUserSuccessTest() throws FailureException {
		User newUser = userAuthService.registerUser("newUser", "newPassword");
		assertEquals(newUser, userRepository.findOne(newUser.getId()));
	}

	@Test
	public void registerUserFailureMissingParameterTest() throws FailureException {
		expectedException.expect(FailureException.class);
	    expectedException.expectMessage(IUserAuthService.MISSING_REQUIRED_PARAMETER);
	    userAuthService.registerUser("newUser", "");
	}

	@Test
	public void registerUserFailureUsernameAlreadyInUse() throws FailureException {
		expectedException.expect(FailureException.class);
	    expectedException.expectMessage(IUserAuthService.USERNAME_ALREADY_IN_USE);
	    String username = "newDemoUser";
		String password = "newDemoPw";
		userAuthService.registerUser(username, password);
	    userAuthService.registerUser(username, password);
	}
}

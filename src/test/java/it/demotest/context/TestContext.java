package it.demotest.context;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import it.demo.configuration.DemoWebAppContext;

@Configuration
@Import({DemoWebAppContext.class, TestEnv.class})
public class TestContext {

}

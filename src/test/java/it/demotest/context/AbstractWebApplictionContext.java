package it.demotest.context;

import java.util.function.Function;

import javax.servlet.Filter;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.demo.security.JWTAuthenticationFilter;
import it.demo.security.TokenHandler;

public abstract class AbstractWebApplictionContext extends TestConfiguration {
	
	public static final String APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON_UTF8_VALUE.toString();
	protected static final String DEMO_USER = "demouser";
	protected static final String DEMO_PW = "pw";
	
	protected MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	private Filter springSecurityFilterChain;
	
	@Autowired
	private TokenHandler tokenHandler;
	
	private String authHeaderValue;

	protected ObjectMapper mapper;
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders
							.webAppContextSetup(webApplicationContext)
							.addFilters(springSecurityFilterChain)
							.build();
		
		this.authHeaderValue = JWTAuthenticationFilter.AUTH_HEADER_PREFIX + tokenHandler.createTokenForUser(DEMO_USER);
		mapper = new ObjectMapper();
	}

	private MockHttpServletRequestBuilder buildAuthenticatedRequest(Function<String, MockHttpServletRequestBuilder> httpMethod, String url) {
		return httpMethod.apply(url)
					.header(JWTAuthenticationFilter.AUTH_HEADER_NAME, authHeaderValue);
	}
	
	protected MockHttpServletRequestBuilder buildAuthenticatedGet(String url) {
		return buildAuthenticatedRequest(MockMvcRequestBuilders::get, url);
	}
}

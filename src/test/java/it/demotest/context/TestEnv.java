package it.demotest.context;

import org.springframework.test.context.ActiveProfiles;

import it.demo.configuration.env.EnvConfigurationDev;

@ActiveProfiles("dev")
public class TestEnv extends EnvConfigurationDev {

}

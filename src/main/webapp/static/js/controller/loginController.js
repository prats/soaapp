'use strict';

demoApp.controller('LoginController', 
					['$scope', '$location', 'AuthenticationService', 
					 function($scope, $location, authenticationService) {

	$scope.error = false;
	authenticationService.clearCredentials();
						
	$scope.login = function () {
        authenticationService.login($scope.username, $scope.password, function(success, message) {
            if(success) {
                $location.path('/home');
            } else {
                $scope.error = message;
            }
        });
    };
		
}]);

'use strict';

demoApp.controller('CustomerController', ['$scope', '$cookies', '$location', 'LocalStorageService', function($scope, $cookies, $location, localStorageService) {

	var addCustomer = function(id, location) {
		return {'id':id, 'location':location};
	}
	
	$scope.customers = [];
	$scope.customers.push(addCustomer(2001, 'London'));
	$scope.customers.push(addCustomer(100, 'Liverpool'));
	$scope.customers.push(addCustomer(-1, 'undefined'));
	
	$scope.startSelection = function(customer) {
		$cookies.put('customerId', customer.id);
		localStorageService.cleanSelectedProducts();
		$location.path('/productsSelection');
	};
		
}]);

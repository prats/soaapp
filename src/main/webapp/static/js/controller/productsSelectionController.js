'use strict';

demoApp.controller('ProductsSelectionController', ['$scope', '$cookies', 'RestApiService', 'LocalStorageService', function($scope, $cookies, restApiService, localStorageService) {

	var customerId = $cookies.get('customerId');
	localStorageService.setCustomerId(customerId);
	
	$scope.sportProducts = [];
	$scope.newsProducts = [];
	
	var prepareModelView = function(products) {
		// dummy model preparation, the categories should be managed dynamically
		for(var i=0; i<products.length; i++) {
			var product = products[i];
			if (product.category.id == 1)
				$scope.sportProducts.push(product);
			else if (product.category.id == 2)
				$scope.newsProducts.push(product);
		}
	};
	
	restApiService
		.getLocationIdByCustomerId(customerId)
		.then(
			function(locationId) {
				return restApiService.getProductsByLocationId(locationId);
			}
		)
		.then(
			function(data) {
				return data;
			},
			function(errResponse){
				alert('Error while fetching products');
			}
		)
		.then(prepareModelView)
	;

	$scope.getSelectedProducts = function(){
		return localStorageService.getSelectedProducts();
	};
	$scope.addSelectedProduct = function(product){
		localStorageService.addSelectedProduct(product);
	};
	$scope.removeSelectedProduct = function(product){
		localStorageService.removeSelectedProduct(product);
	};
	$scope.hasPreviousSelection = function(product){
		var selected = localStorageService.isProductSelected(product);
		product.selected = selected;
		return selected;
	};
	$scope.updateBasket = function(product) {
		if(product.selected)
			$scope.addSelectedProduct(product);
		else
			$scope.removeSelectedProduct(product);
	};
	
}]);

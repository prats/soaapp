'use strict';

demoApp.controller('ConfirmationController', ['$scope', 'LocalStorageService', function($scope, localStorageService) {

	$scope.customerId = localStorageService.getCustomerId();
	$scope.selectedProducts = localStorageService.getSelectedProducts();
	$scope.hasSelection = $scope.selectedProducts.length > 0; 	
}]);

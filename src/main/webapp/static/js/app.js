'use strict';

var demoApp = angular.module('demoApp', ['ui.bootstrap','ngRoute', 'ngCookies', 'ngStorage', 'ngMaterial']);

var pagesPath = '../static/pages/';
//var	pagesPath = './pages/'; //tomcat direct

demoApp.config(function($routeProvider) {
	$routeProvider.
		when('/login', {templateUrl: pagesPath + 'login.html'}).
	    when('/home', {templateUrl: pagesPath + 'customer.html'}).
	    when('/productsSelection', {templateUrl: pagesPath + 'productsSelection.html'}).
	    when('/confirmation', {templateUrl: pagesPath + 'confirmation.html'}).
	    otherwise({redirectTo: '/login'});
});

demoApp.directive( 'goClick', function ( $location ) {
	  return function(scope, element, attrs) {
		var path;

		attrs.$observe('goClick', function(val) {
			path = val;
		});

		element.bind('click', function() {
			scope.$apply(function() {
				$location.path(path);
			});
		});
	};
});

run.$inject = ['$rootScope', '$location', '$cookies', '$http', '$mdDialog'];
function run($rootScope, $location, $cookies, $http, $mdDialog) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookies.getObject('globals') || {};
    if ($rootScope.globals.currentUser)
    	 $http.defaults.headers.common.Authorization = $rootScope.globals.currentUser.auth;

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['', '/login', '/register']) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
        	event.preventDefault();
        	var alert = $mdDialog.alert({
        		title: 'Attention',
        		textContent: 'This page needs authentication',
        		ok: 'Go to login'
        	});

        	$mdDialog
	        	.show( alert )
	        	.finally(function() {
	        		$location.path('/login');
	        	});
        }
    });
}
demoApp.run(run);

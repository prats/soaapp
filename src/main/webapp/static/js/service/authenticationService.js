'use strict';
  
demoApp
.factory('AuthenticationService', ['$rootScope', '$http', '$cookies', 'RestApiService',
    function ($rootScope, $http, $cookies, restApiService) {
        var service = {};
 
        service.login = function (username, password, loginCallback) {
            return restApiService.login(username, password, loginCallback)
            			.then (function (response) {
            						var data = response.data; 
				                	$rootScope.globals = {
			                            currentUser: {
			                                username: username,
			                                token: data.jwt,
			                                auth: 'Bearer ' + data.jwt
			                            }
			                        };
			             
			                        $http.defaults.headers.common.Authorization = $rootScope.globals.currentUser.auth;
			                        $cookies.putObject('globals', $rootScope.globals);

					                loginCallback(true, data.message);
            					}
            			);
        };
        
        service.clearCredentials = function() {
            $rootScope.globals = {};
            $cookies.remove('globals');
            $http.defaults.headers.common.Authorization = false;
        }
        
        service.isAuthenticated = function() {
        	// TODO check expiration
        	return true;
        };
  
        return service;
    }]
);
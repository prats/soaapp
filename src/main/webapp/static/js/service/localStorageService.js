'use strict';

demoApp
.factory('LocalStorageService', ['$localStorage', function($localStorage){
	
	if ($localStorage.selectedProducts === undefined)
		$localStorage.selectedProducts = [];
	
	return {

		setCustomerId: function(customerId){
			$localStorage.customerId = customerId;
		},
		
		getCustomerId: function(){
			return $localStorage.customerId;
		},
		
		addSelectedProduct: function(product){
			$localStorage.selectedProducts.push(product);
		},
		
		removeSelectedProduct: function(product){
			var array = $localStorage.selectedProducts;
			for(var i=0; i<array.length; i++) {
			    if(array[i].id === product.id)
			       array.splice(i, 1);
			}
		},
		
		getSelectedProducts: function(){
			return $localStorage.selectedProducts;
		},
		
		isProductSelected: function(product){
			var array = $localStorage.selectedProducts;
			for(var i=0; i<array.length; i++) {
			    if(array[i].id === product.id)
			      return true;
			}
			return false;
		},
		
		cleanSelectedProducts: function(){
			return $localStorage.selectedProducts = [];
		},
		
	}
	
}]);
'use strict';

demoApp
.factory('RestApiService', ['$http', '$q', function($http, $q){

	var contextPath = 'http://localhost:8180/Demo/api/';
	
	return {

		getLocationIdByCustomerId: function(customerId){
			return $http
			.get(contextPath + 'getLocationIdByCustomerId', {params: {customerId:customerId}})
			.then(
				function(response){
					return response.data.locationId;
				}, 
				function(errResponse){
					return "undefined";
				}
			);
		},
		
		getProductsByLocationId: function(locationId){
			// caching policy could be helpful 
			return $http
					.get(contextPath + 'getProductsByLocationId', {params: {locationId:locationId}})
					.then(
						function(response){
							return response.data;
						}, 
						function(errResponse){
							console.error('Error while getting products for locationId: ' + locationId);
							return $q.reject(errResponse);
						}
					);
	    },
	    
	    login : function(username, password, loginCallback) {
	    	var data = {
	    		username: username, 
	    		password: password
            };

            var config = {
                headers : {
                	'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }

	    	return $http
					.post(contextPath + 'public/login', data, config)
					.then(
						function(response){
							return response;
						}, 
						function(errResponse){
							var msg;
							if (errResponse.status === 401)
								msg = errResponse.data.message;
							else
								msg = 'Unable to call login service';
							loginCallback(false, msg);
							return $q.reject(errResponse);
						}
					);
	    }
		
	};

}]);

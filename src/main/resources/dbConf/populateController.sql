insert into Category values (1, 'Sports', 0);
insert into Category values (2, 'News', 0);

insert into Location values ('ENG-LON', 'London', 0);
insert into Location values ('ENG-LIV', 'Liverpool', 0);

insert into Product (description, optlock, category_id, location_id) values ('Chelsea TV', 0, 1, 'ENG-LON');
insert into Product (description, optlock, category_id, location_id) values ('Arsenal TV', 0, 1, 'ENG-LON'); 
insert into Product (description, optlock, category_id, location_id) values ('Liverpool TV', 0, 1, 'ENG-LIV');

insert into Product (description, optlock, category_id, location_id) values ('Sky Sports News', 0, 2, null);
insert into Product (description, optlock, category_id, location_id) values ('Sky News', 0, 2, null);

insert into User (id, optlock, username, password) values (1, 0, 'demouser', 'GpHWL3ymc5liWkNopqtdSjuqYHM=');

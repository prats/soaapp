package it.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
    @Column(name="optlock", nullable=false)
	private Long optlock;

	@ManyToOne(optional=false, fetch=FetchType.EAGER)
	private Category category;
	
	@Column(name = "description", nullable=false)
	private String description;
	
	@ManyToOne(optional=true)
	private Location location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOptlock() {
		return optlock;
	}

	public void setOptlock(Long optlock) {
		this.optlock = optlock;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(id)
			.append(category)
			.append(description)
			.append(location);
		return builder.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Product) {
			Product other = (Product) obj;
	        EqualsBuilder builder = new EqualsBuilder();
	        builder.append(getId(), other.getId());
	        builder.append(getCategory(), other.getCategory());
	        builder.append(getDescription(), other.getDescription());
	        builder.append(getLocation(), other.getLocation());
	        return builder.isEquals();
	    }
	    return false;
	}
}

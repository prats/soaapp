package it.demo.model.ext;

public class LoginOutcome {
	private boolean success;
	private String message;
	private String jwt;

	public LoginOutcome() {
		this.success = false;
		this.message = "suka";
		this.jwt = "test";
	}
	
	public LoginOutcome(boolean success, String message, String jwt) {
		this.success = success;
		this.message = message;
		this.jwt = jwt;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public String getJwt() {
		return jwt;
	}
	
}

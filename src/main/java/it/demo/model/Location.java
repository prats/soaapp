package it.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Location {
	@Id
	private String id;
	
	@Version
    @Column(name="optlock", nullable=false)
	private Long optlock;

	@Column(name = "description", nullable=false)
	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getOptlock() {
		return optlock;
	}

	public void setOptlock(Long optlock) {
		this.optlock = optlock;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(id)
			.append(description);
		return builder.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Product) {
			Product other = (Product) obj;
	        EqualsBuilder builder = new EqualsBuilder();
	        builder.append(getId(), other.getId());
	        builder.append(getDescription(), other.getDescription());
	        return builder.isEquals();
	    }
	    return false;
	}	
}

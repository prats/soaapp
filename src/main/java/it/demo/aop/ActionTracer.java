package it.demo.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ActionTracer {
	
	private static final Logger logger = Logger.getLogger(ActionTracer.class);
	
	@After("execution(* it.demo.repositories.*.save*(..))")
	public void log(JoinPoint joinPoint) {
		logger.info("called " + joinPoint.getSignature().getName() + " on " + joinPoint.getArgs()[0].getClass().getSimpleName());
	}
}

package it.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.demo.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	public User findByUsername(String username);
}

package it.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.demo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	
	@Query("SELECT p "
			+ "FROM Product p "
			+ "WHERE p.location = null "
			+ 		"OR p.location.id = :locationId "
			+ "ORDER BY p.category.id, p.description")
	public List<Product> findByLocationIdOrNotLocalized(@Param("locationId") String locationId);
}

package it.demo.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("it.demo.repositories")
public class DatabaseConfiguration {
	
	@Autowired
	private IEnvConfiguration conf; 
	
	@Bean
	public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
	    final DataSourceInitializer initializer = new DataSourceInitializer();
	    initializer.setDataSource(dataSource);
	    return initializer;
	}

	@Bean
	public DataSource dataSource() {
		return conf.dataSource();
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
	    JpaTransactionManager transactionManager = new JpaTransactionManager();
	    transactionManager.setEntityManagerFactory(entityManagerFactory);
	    return transactionManager;
	}
	
	private JpaVendorAdapter createJpaVendorAdapter() {
		return conf.createJpaVendorAdapter();
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(Environment env) throws Exception {
	    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
	    factory.setPersistenceUnitName("persistenceUnit");
	    factory.setJpaVendorAdapter(createJpaVendorAdapter());
	    factory.setPackagesToScan("it.demo.model");
	    factory.setDataSource(dataSource());     
	    factory.setJpaProperties(jpaProperties());
	    factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());

	    return factory;
	}

	private Properties jpaProperties() {
	    Properties properties = new Properties();
	    properties.setProperty("hibernate.dialect", conf.getProperty("hibernate.dialect"));
		properties.setProperty("hibernate.default_schema", conf.getProperty("hibernate.default_schema"));
		properties.setProperty("hibernate.hbm2ddl.auto", conf.getProperty("hibernate.hbm2ddl.auto"));
		properties.setProperty("hibernate.hbm2ddl.import_files", conf.getProperty("hibernate.hbm2ddl.import_files"));
		properties.setProperty("hibernate.show_sql", conf.getProperty("hibernate.show_sql"));
		properties.setProperty("hibernate.format_sql", conf.getProperty("hibernate.format_sql"));

	    return properties;
	}
	
}

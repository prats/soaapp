package it.demo.configuration;

import javax.sql.DataSource;

import org.springframework.orm.jpa.JpaVendorAdapter;

public interface IEnvConfiguration {
	public static final String DEV = "dev";
	
	public static final String URL_API = "/api";
	public static final String URL_PUBLIC_API = "/api/public";

	public static final String URL_STATIC_RESOURCES = "/static";

	public DataSource dataSource();
	public JpaVendorAdapter createJpaVendorAdapter();
	public String getProperty(String key);
}

package it.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import it.demo.security.CORSFilter;
import it.demo.security.JWTAuthenticationFilter;
import it.demo.security.TokenHandler;
import it.demo.security.UserService;

@Configuration
@EnableWebSecurity
@Order(2)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserService userService;

    public SecurityConfig() {
        super(true);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .exceptionHandling().and()
            .anonymous().and()

            .authorizeRequests()
            .antMatchers(IEnvConfiguration.URL_PUBLIC_API + "/**").permitAll()				//allow public api including login
            .antMatchers(HttpMethod.OPTIONS, IEnvConfiguration.URL_API + "/**").permitAll()	//allow CORS option calls
            .antMatchers(IEnvConfiguration.URL_STATIC_RESOURCES + "/**").permitAll()					//allow static resources

            .anyRequest().authenticated().and()

            .addFilterBefore(new JWTAuthenticationFilter(tokenHandler(), userService),	//jwt authentication
                    UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(new CORSFilter(),		//allow cors request managing preflight request (OPTIONS method)
            		JWTAuthenticationFilter.class)
            ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Bean
    public TokenHandler tokenHandler() {
        return new TokenHandler("monkeyPassword");
    }
}
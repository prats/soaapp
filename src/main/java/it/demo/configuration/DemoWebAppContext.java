package it.demo.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "it.demo")
@Import({DatabaseConfiguration.class, SecurityConfig.class})
public class DemoWebAppContext extends WebMvcConfigurerAdapter{
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(IEnvConfiguration.URL_STATIC_RESOURCES + "/**")
				.addResourceLocations(IEnvConfiguration.URL_STATIC_RESOURCES + "/");
	}

}
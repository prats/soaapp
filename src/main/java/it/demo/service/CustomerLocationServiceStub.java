package it.demo.service;

import it.demo.model.exception.FailureException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("customerLocationServiceStub")
public class CustomerLocationServiceStub implements ICustomerLocationService {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(CustomerLocationServiceStub.class);

	public static final String LOCATION_LIVERPOOL = "ENG-LIV";
	public static final String LOCATION_LONDON = "ENG-LON";

	@Override
	public String getCustomerLocationId(long customerId) throws FailureException {
		if (customerId < 0)
			throw new FailureException();
		
		if (customerId < 1000)
			return LOCATION_LIVERPOOL;
		
		return LOCATION_LONDON;
	}
}

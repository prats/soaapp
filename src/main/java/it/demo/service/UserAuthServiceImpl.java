package it.demo.service;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import it.demo.model.User;
import it.demo.model.exception.FailureException;
import it.demo.model.ext.LoginOutcome;
import it.demo.repositories.UserRepository;
import it.demo.security.PasswordUtility;
import it.demo.security.TokenHandler;

@Service("loginService")
public class UserAuthServiceImpl implements IUserAuthService {
	private static final Logger logger = Logger.getLogger(UserAuthServiceImpl.class);

	private static final LoginOutcome failureInvalidUser = new LoginOutcome(false, "invalid user", null);
	private static final LoginOutcome failureInvalidPw = new LoginOutcome(false, "invalid password", null);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TokenHandler tokenHandler;
	
	@Override
	public LoginOutcome login(String username, String password) {
		User user = userRepository.findByUsername(username);
		if (user == null)
			return failureInvalidUser;

		try {
			if (!user.getPassword().equals(PasswordUtility.encrypt(password)))
				return failureInvalidPw;
		} catch (Exception e) {
			logger.info(e);
			return failureInvalidPw;
		}

		return new LoginOutcome(true, "success", tokenHandler.createTokenForUser(username));
	}

	@Override
	@Transactional
	public User registerUser(String username, String password) throws FailureException {
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
			throw new FailureException(MISSING_REQUIRED_PARAMETER);

		if (userRepository.findByUsername(username) != null)
			throw new FailureException(USERNAME_ALREADY_IN_USE);

		User user = new User();
		user.setUsername(username);
		try {
			user.setPassword(PasswordUtility.encrypt(password));
		} catch (Exception e) {
			throw new FailureException(GENERIC_ERROR);
		}
		return userRepository.save(user);
	}
}

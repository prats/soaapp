package it.demo.service;

import java.util.List;

import it.demo.model.Product;

public interface ICatalogueService {
	public List<Product> getProducts(String locationId);
}

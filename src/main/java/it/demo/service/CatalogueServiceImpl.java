package it.demo.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.demo.model.Product;
import it.demo.repositories.ProductRepository;

@Service("catalogueService")
public class CatalogueServiceImpl implements ICatalogueService {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(CatalogueServiceImpl.class);
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	@Transactional(readOnly=true)
	public List<Product> getProducts(String locationId) {
		return productRepository.findByLocationIdOrNotLocalized(locationId);
	}

}

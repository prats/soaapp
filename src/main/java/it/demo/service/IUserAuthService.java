package it.demo.service;

import it.demo.model.User;
import it.demo.model.exception.FailureException;
import it.demo.model.ext.LoginOutcome;

public interface IUserAuthService {
	public static final String USERNAME_ALREADY_IN_USE = "username already in use";
	public static final String MISSING_REQUIRED_PARAMETER = "missing required parameter";
	public static final String GENERIC_ERROR = "generic error";

	public LoginOutcome login(String username, String password);
	public User registerUser(String username, String password) throws FailureException;
}

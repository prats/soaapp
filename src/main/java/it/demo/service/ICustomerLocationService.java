package it.demo.service;

import it.demo.model.exception.FailureException;

public interface ICustomerLocationService {
	public String getCustomerLocationId(long customerId) throws FailureException;
}

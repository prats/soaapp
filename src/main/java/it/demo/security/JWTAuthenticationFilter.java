package it.demo.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.filter.GenericFilterBean;

public class JWTAuthenticationFilter extends GenericFilterBean {
	
	public static final String AUTH_HEADER_NAME = "Authorization";
	public static final String AUTH_HEADER_PREFIX = "Bearer ";

    private final TokenHandler tokenHandler;
    private final UserService userService;

    public JWTAuthenticationFilter(TokenHandler tokenHandler, UserService userService) {
		this.tokenHandler = tokenHandler;
		this.userService = userService;
	}

	@Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Authentication authentication = getAuthentication(httpRequest);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
        SecurityContextHolder.getContext().setAuthentication(null);
    }
    
    public Authentication getAuthentication(HttpServletRequest request) {
        final String token = request.getHeader(AUTH_HEADER_NAME);
        if (token != null && token.startsWith(AUTH_HEADER_PREFIX)) {
            String username = tokenHandler.parseUserFromToken(token.replace(AUTH_HEADER_PREFIX, ""));
			final User user = userService.buildUserByUsername(username);
            if (user != null) {
                return new UserAuthentication(user);
            }
        }
        return null;
    }
}
package it.demo.security;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public final class TokenHandler {

    private final String secret;

    public TokenHandler(String secret) {
        this.secret = secret;
    }

    public String parseUserFromToken(String token) {
        String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return username;
    }

    public String createTokenForUser(String username) {
    	LocalDateTime expire = LocalDateTime.now().plusDays(1);
    	Instant instant = expire.atZone(ZoneId.systemDefault()).toInstant();
    	Date expireDate = Date.from(instant);
        return Jwts.builder()
                .setSubject(username)
                .signWith(SignatureAlgorithm.HS512, secret)
                .setExpiration(expireDate)
                .compact();
    }
}
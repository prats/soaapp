package it.demo.security;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import it.demo.repositories.UserRepository;

@Service
public class UserService {

    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();
    
    @Autowired
	private UserRepository userRepository;

    public final User buildUserByUsername(String username) throws UsernameNotFoundException {
    	if (userRepository.findByUsername(username) == null)
    		return null;
        User user = new User(username, "empty", Collections.emptyList());
        detailsChecker.check(user);
        return user;
    }

}

package it.demo.security;

import java.security.MessageDigest;
import java.util.Base64;

public class PasswordUtility {

	public static synchronized String encrypt(String pw) throws Exception {
		MessageDigest md = null;
		md = MessageDigest.getInstance("SHA");
		md.update(pw.getBytes("UTF-8"));

		return (Base64.getEncoder().encodeToString(md.digest()));
	}

	public static void main(String[] args) throws Exception {
		System.out.println(PasswordUtility.encrypt("pw"));
	}
}


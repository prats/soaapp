package it.demo.controller;
 
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.demo.configuration.IEnvConfiguration;
import it.demo.model.exception.FailureException;
import it.demo.model.ext.Credentials;
import it.demo.model.ext.LoginOutcome;
import it.demo.service.IUserAuthService;
 
@RequestMapping(IEnvConfiguration.URL_PUBLIC_API)
@RestController
public class RestPublicEndpointController {
	private static final Logger logger = Logger.getLogger(RestPublicEndpointController.class);
 
    @Autowired
    private IUserAuthService userAuthService;
    
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = {"application/json"}, headers = {"content-type=application/json"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<LoginOutcome> login(@RequestBody Credentials credentials) {
        logger.info("Call to login:" + credentials.getUsername() + " " + credentials.getPassword());
        LoginOutcome loginOutcome = userAuthService.login(credentials.getUsername(), credentials.getPassword());
        HttpStatus code = HttpStatus.UNAUTHORIZED;  
        if (loginOutcome.isSuccess())
        	code = HttpStatus.OK;
		return new ResponseEntity<LoginOutcome>(loginOutcome, code);
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = {"application/json"}, headers = {"content-type=application/json"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> register(@RequestBody Credentials credentials) {
        logger.info("Call to register:" + credentials.getUsername() + " " + credentials.getPassword());
        HttpStatus code;
        String msg;
        try {
			userAuthService.registerUser(credentials.getUsername(), credentials.getPassword());
			code = HttpStatus.OK;
			msg = "success";
		} catch (FailureException e) {
			code = HttpStatus.BAD_REQUEST;
			msg = e.getMessage();
		}
		return new ResponseEntity<String>(msg, code);
    }
    
    @RequestMapping(value = "/alive", method = RequestMethod.GET)
    public ResponseEntity<String> alive() {
    	return new ResponseEntity<String>("alive", HttpStatus.OK);
    }
}
package it.demo.controller;
 
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.demo.configuration.IEnvConfiguration;
import it.demo.model.Product;
import it.demo.model.exception.FailureException;
import it.demo.service.ICatalogueService;
import it.demo.service.ICustomerLocationService;
 
@RequestMapping(IEnvConfiguration.URL_API)
@RestController
public class RestEndpointController {
	private static final Logger logger = Logger.getLogger(RestEndpointController.class);
 
    @Autowired
    private ICatalogueService catalogueService;
    
    @Autowired
    private ICustomerLocationService customerLocationService;
    
    @RequestMapping(value = "/getLocationIdByCustomerId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Map<String,String>> getLocationIdByCustomerId(@RequestParam("customerId") long customerId) {
        String outcome = null;
        HttpStatus httpStatus;
        try {
			outcome = customerLocationService.getCustomerLocationId(customerId);
			httpStatus = HttpStatus.OK;
		} catch (FailureException e) {
			httpStatus = HttpStatus.NOT_FOUND;
		}
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.info("Call to getLocationIdByCustomerId api with customerId:" + customerId + " and outcome:" + outcome);
        return new ResponseEntity<Map<String,String>>(Collections.singletonMap("locationId", outcome), httpStatus);
    }
 
    @RequestMapping(value = "/getProductsByLocationId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Product>> getProductsByLocationId(@RequestParam("locationId") String locationId) {
        List<Product> products = catalogueService.getProducts(locationId);
        logger.info("Call to getProductsByLocationId api with locationId:" + locationId + " and returned #products:" + products.size());
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    } 

    @RequestMapping(value = "/alive", method = RequestMethod.GET)
    public ResponseEntity<String> alive() {
    	return new ResponseEntity<String>("alive", HttpStatus.OK);
    }
}